s = []  # stack
o = {   # operators
  '+':lambda y,x:x+y,
  '-':lambda y,x:x-y,
  '*':lambda y,x:x*y,
  '=':lambda y,x:x==y,
  '~':lambda y,x:x!=y,
  '^':lambda y,x:x**y,
  '|':lambda y,x:max(x,y),
  '&':lambda y,x:min(x,y),
  '%':lambda y,x:x/y
}

while True:
  for t in input().split():
    s.append(int(o[t](s.pop(),s.pop()) if t in o else int(t)))
  print(s[-1])
