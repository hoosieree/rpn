NB. Python has an input() function.  J doesn't.
NB. J does have a way to set the next phrase for execution, and you can
NB. tell the interpreter to go into that mode, for similar functionality.
readln =: [: (1!:1) 1:
donext =: [: (9!:29) 1: [ 9!:27

isnum =: ((0$0)-:[:$".@":) S:0
stack =: 0$0
verbs =: '+ - * %'
in =: 4 :'(>x) e. y'

parse =: verb define
  NB. tokens =. 0$0

  i =. 0
  s =. cut y
  while. i<#s do.
    if. (i{s) in verbs do.
      'pop rest' =. _2 split stack
      stack =: rest
      push =. <":(i{s)/".>pop
      stack =: stack, push
    else.
      stack =: stack, i{s
    end.
    i =. >:i
  end.
  stack
)

main =: 3 :0  NB. for input, if isnum, push, else pop2, eval, push
  echo LF, 'main loop. type ''bye'' to exit.'
  while. (s:'`bye') ~: s:<input=:readln'' do.
    echo parse input
  end.
)

donext 'main _'