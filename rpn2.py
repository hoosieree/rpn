# RPN calculator, now with variables!

# String -> Number
def num(s):
    try: return int(s)
    except ValueError: return float(s)

# [Word] -> Names -> Dyads -> [[TokenType,Value]]
def parse(words, names, dyads):
    i = 0
    tokens = []
    while i < len(words):
        # ':' is assignment expression (pushes value to stack)
        # '::' is assignment statement (stack unchanged)
        if (words[i] == ':') or (words[i] == '::'):
            name = words[i+1]
            names[name] = words[i+2]
            i+=len(words[i])
        elif words[i] in dyads:
            tokens.append(['dyad', words[i]])
        elif words[i] in names:
            # recursive parsing - maybe unnecessary at this stage...
            tokens.append(parse([names[words[i]]], names, dyads)[0])
        else:
            tokens.append(['literal', num(words[i])])
        i+=1
    return tokens

# Stack -> [String] -> IO()
def evaluate(s, t, dyads):
    # print(t)
    for k, v in t:
        if k == 'literal':
            s.append(v)
        elif k == 'dyad':  # apply Dyad: pop two, push result
            s.append(dyads[v](s.pop(), s.pop()))
    return s

# GLOBALS
STACK = []
RPN_NAMES = {} # namespace for variables
RPN_DYAD = {
    '%': lambda y,x: x/y,
    '&': lambda y,x: min(x,y),
    '*': lambda y,x: x*y,
    '+': lambda y,x: x+y,
    '-': lambda y,x: x-y,
    '=': lambda y,x: int(x==y),
    '^': lambda y,x: x**y,
    '|': lambda y,x: max(x,y),
    '~': lambda y,x: int(x!=y),
}

def run():
    while True:
        tokens = parse(input('   ').split(), RPN_NAMES, RPN_DYAD)
        evaluate(STACK, tokens, RPN_DYAD)
        print(STACK, RPN_NAMES)

if __name__ == "__main__":
    run()
